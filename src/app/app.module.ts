import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {ChoicePage} from "../pages/choiceCaissier/choice";
import {ChoiceWaiterPage} from "../pages/choice-waiter/choice-waiter";
import {LoginPage} from "../pages/login/login";
import {ScanPage} from "../pages/scan/scan";
import {SoldeRechargePage} from "../pages/solde-recharge/solde-recharge";
import {NewCartePage} from "../pages/new-carte/new-carte";
import {CashoutPage} from "../pages/cashout/cashout";
import {FacturePage} from "../pages/facture/facture";
import {CheckPage} from "../pages/check/check";
import {ChoiceManagerPage} from "../pages/choice-manager/choice-manager";
import { IonDigitKeyboard } from '../components/ion-digit-keyboard/ion-digit-keyboard.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ScanPage,
    CashoutPage,
    SoldeRechargePage,
    NewCartePage,
    ChoicePage,
    ChoiceWaiterPage,
    ChoiceManagerPage,
    FacturePage,
    CheckPage
  ],
  imports: [
    BrowserModule, HttpModule,
    IonDigitKeyboard,
    IonicModule.forRoot(MyApp),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ScanPage,
    CashoutPage,
    SoldeRechargePage,
    NewCartePage,
    ChoicePage,
    ChoiceWaiterPage,
    ChoiceManagerPage,
    FacturePage,
    CheckPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
