import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import {ScanPage} from "../scan/scan";
import {NewCartePage} from "../new-carte/new-carte";
import {CheckPage} from "../check/check";

/**
 * Generated class for the ChoiceManagerPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-choice-manager',
  templateUrl: 'choice-manager.html',
})
export class ChoiceManagerPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoiceManagerPage');
  }
  redirectRecharge(){
    this.navCtrl.push(ScanPage);
  }
  redirectAdd(){
    this.navCtrl.push(NewCartePage);
  }
  redirectCheck(){
    this.navCtrl.push(CheckPage);
  }

}
