import {Component, ViewChild} from '@angular/core';
import {AlertController, NavController, NavParams, ViewController} from 'ionic-angular';
import {NFC} from "@ionic-native/nfc";
import {ChoiceWaiterPage} from "../choice-waiter/choice-waiter";
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {IonDigitKeyboardCmp, IonDigitKeyboardOptions} from '../../components/ion-digit-keyboard';

/**
 * Generated class for the FacturePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-facture',
    templateUrl: 'facture.html',
    providers: [NFC]
})

export class FacturePage {
    sumToPayTotale: string = '';
    sumToPayPartial: string = '';
    tip = '';
    orderId;
    defineFacture = false;
    newCard = false;
    done = false;
    tipTopay = false;
    detectedf = false;
    detectedt = false;
    tagId = '';
    sumKey = '';
    totale = '';
    ipLocal = 'http://cashless.socketon.xyz/api/';
    textPay = 'Payez';
    textPayTip = 'Scan carte';
    textPayScreen = this.textPay;
    textTipScreen = this.textPayTip;
    waitingText = 'Encours de traitement ..';
    waitingTextScan = 'En attente d\'une carte'
    clickedPayT = false;
    disabledT =false;
    disabledP = false;
    //header = new Headers({ 'Authorization' : 'Bearer '+localStorage.getItem('token')});
    options = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')})});

    constructor(private http: Http, public navCtrl: NavController, public navParams: NavParams,
                public nfc: NFC, private viewCtrl: ViewController, public alertCtrl: AlertController) {
        this.nfc = nfc;
        this.http = http;
    }

    @ViewChild(IonDigitKeyboardCmp) keyboard;

    userId: string = '';
    userPassword: string = '';
    focus: string = '';

    public keyboardSettings: IonDigitKeyboardOptions = {
        align: 'center',
        //width: '85%',
        visible: true,
        leftActionOptions: {
            iconName: 'ios-backspace-outline',
            fontSize: '1.4em'
        },
        rightActionOptions: {
            iconName: 'ios-checkmark-circle-outline',
            // text: '.',
            fontSize: '1.3em'
        },
        roundButtons: false,
        showLetters: true,
        swipeToHide: false,
        // Available themes: IonDigitKeyboard.themes
        theme: 'dark'
    }

    ionViewWillEnter(){
        /**
         * Since we want to prevent native keyboard to show up, we put the disabled
         * attribute on the input, and manage focus programmaticaly.
         */
        this.keyboard.onClick.subscribe((key: any) => {
            console.log(key)
            if (typeof key == 'number') {
                this.sumKey += key;
                this.changeSum();
            } else {
                if (key == 'left') this.sumKey = this.sumKey.substring(0, this.sumKey.length - 1);
                this.changeSum();
            }
        });

        // (BLur) Clear focus field name on keyboard hide
        this.keyboard.onHide.subscribe(() => {
            this.focus = '';
        });
    }

    changeSum() {
        if (!this.defineFacture)
            this.sumToPayTotale = this.sumKey;
        else if (!this.tipTopay)
            this.sumToPayPartial = this.sumKey;
        else
            this.tip = this.sumKey
    }

    setFocus(field: string) {
        this.focus = field;
        this.keyboard.show();
    }

    createOrder() {
        // to test just
        // this.defineFacture = true;
        // this.sumKey = '';
        var body = {
            amount: parseInt(this.sumToPayTotale) * 1000
        }
        //console.log(this.options)
        this.http.post(this.ipLocal + 'orders/create', body, this.options).map(res => res.json()).subscribe(
            data => {
                console.log(data);
                this.defineFacture = true;
                this.orderId = data.data.order.id;
                this.sumKey = '';
                console.log(this.orderId)
                this.totale = this.sumToPayTotale+" , 000 DT";
            },
            error => {
                let errorData = JSON.parse(error._body);
                let alert = this.alertCtrl.create({
                    title: 'ERREUR',
                    subTitle:errorData.data.message,
                    buttons: ['OK']
                });
                alert.present();

            }
        );
    }

    payWithNewCarte() {
        this.newCard = true;
    }

    nfcTag() {
        this.textPayScreen = this.waitingTextScan;
        if (this.sumToPayPartial == '') {
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: 'SVP tapez le montant',
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            this.disabledP = true;
            this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
                if (data && data.tag && data.tag.id) {
                    let tagId = this.nfc.bytesToHexString(data.tag.id);
                    if (tagId) {
            console.log('before pay')
                this.showTag(tagId);
                // this.showTag('dee22bbc');
                this.textPayScreen = this.waitingText;
                    }
                }
            });
        }
    }

    showTag(tagId) {

        console.log(this.sumToPayTotale);
        this.tagId = tagId
        var body = {
            amount: parseInt(this.sumToPayPartial) * 1000,
            cardRealId: this.tagId
        }
        console.log(this.tagId)
        if (this.tagId != '') {
            this.http.post(this.ipLocal + 'orders/' + this.orderId + '/pay', body, this.options).map(res => res.json()).subscribe(
                data => {
                    console.log(data)
                    if (data.code == 74) {
                        console.log(data);
                        this.defineFacture = true;
                        this.totale = data.data.order.restToPayAmount;
                        this.tipTopay = true;
                        this.done = data.data.order.isPaid;
                        this.sumToPayPartial = '';
                        this.sumKey = '';
                        this.tagId = '';
                        this.disabledP=false;
                        this.textPayScreen = this.textPay;
                        let alert = this.alertCtrl.create({
                            title: 'Succès',
                            subTitle: 'Montant payé',
                            buttons: ['OK']
                        });
                        alert.present();
                    }
                },
                error => {
                    let errorData = JSON.parse(error._body);
                    let alert = this.alertCtrl.create({
                        title: 'ERREUR',
                        subTitle:errorData.data.message,
                        buttons: ['OK']
                    });
                    alert.present();
                    this.disabledP=false;
                    this.textPayScreen = this.textPay;
                }
            );
        } else {
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: 'Scanner une carte SVP',
                buttons: ['OK']
            });
            alert.present();
        }
        this.nfc.removeTagDiscoveredListener(this.showTag)

    }

    nfcTagTip() {

        if (this.tip == '') {
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: 'SVP tapez le montant',
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            this.disabledT = true;
            this.textTipScreen = this.waitingTextScan;
            this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
                    if (data && data.tag && data.tag.id) {
                        let tagId = this.nfc.bytesToHexString(data.tag.id);
                        if (tagId) {
                            this.showTagTip(tagId);
                            // this.showTagTip('dee22bbc');
                            this.textTipScreen = this.waitingText;

                        }
                    }
                }
            );
        }
    }

    showTagTip(tagId) {
        this.tagId = tagId
        var body = {
            amount: parseInt(this.tip) * 1000,
            cardRealId: this.tagId
        }
        if (this.tagId != '') {
            this.http.post(this.ipLocal + 'tips/pay', body, this.options).map(res => res.json()).subscribe(
                data => {
                    console.log(data);
                    console.log(this.done);
                    if (data.code == 74) {
                        this.defineFacture = true;
                        this.tipTopay = false;
                        this.tip = '';
                        this.disabledT = false;
                        if (this.done){
                            console.log('done');
                            this.defineFacture = false;
                            this.navCtrl.push(ChoiceWaiterPage).then(() => {
                                // first we find the index of the current view controller:
                                const index = this.viewCtrl.index;
                                // then we remove it from the navigation stack
                                this.navCtrl.remove(index);
                            })
                        }
                        this.sumToPayPartial = '';
                        this.sumKey = '';
                        this.tagId = '';
                        this.textTipScreen = this.textPayTip;
                        console.log(this.sumToPayTotale);
                        let alert = this.alertCtrl.create({
                            title: 'Succès',
                            subTitle: 'Pouboire payé',
                            buttons: ['OK']
                        });
                        alert.present();

                    }
                },
                error => {
                    let errorData = JSON.parse(error._body);
                    let alert = this.alertCtrl.create({
                        title: 'ERREUR',
                        subTitle:errorData.data.message,
                        buttons: ['OK']
                    });
                    alert.present();
                    this.disabledT = false;
                    this.textTipScreen = this.textPayTip;
                }
            );
        } else {
            let alert = this.alertCtrl.create({
                title: 'Alert',
                subTitle: 'Scanner une carte SVP',
                buttons: ['OK']
            });
            alert.present();
        }
        this.nfc.removeTagDiscoveredListener(this.showTag)
    }

    sesReadNFC(data): void {
        // alert('NFC_WORKING'+data);
    }

    noTip() {
        this.tipTopay = false;
    }

    finish() {
        this.navCtrl.push(ChoiceWaiterPage).then(() => {
            // first we find the index of the current view controller:
            const index = this.viewCtrl.index;
            // then we remove it from the navigation stack
            this.navCtrl.remove(index);
        })
    }
}
