import { Component } from '@angular/core';
import {AlertController, NavController, NavParams, ViewController} from 'ionic-angular';
import {ChoicePage} from "../choiceCaissier/choice";
import {NFC} from "@ionic-native/nfc";
import {Http, Headers, RequestOptions} from "@angular/http";

/**
 * Generated class for the CheckPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-check',
  templateUrl: 'check.html',
  providers : [NFC]
})
export class CheckPage {
  id;
  textToShow = '';
  textToShow1= '';
  showSum = false;
  sum = 0;
  bonusSum = 0;
  expired;
  realId;
  tagId = '';
  ipLocal = 'http://cashless.socketon.xyz/api/';
  options = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')})});

  constructor(private viewCtrl: ViewController,public navCtrl: NavController,public alertCtrl: AlertController,
              public navParams: NavParams,public nfc : NFC,private http:Http) {
    this.nfc = nfc;
    this.http = http;
  }

  ionViewWillEnter() {
    this.nfcTag();
  }

  end(){
    this.navCtrl.push(ChoicePage).then(() => {
      // first we find the index of the current view controller:
      const index = this.viewCtrl.index;
      // then we remove it from the navigation stack
      this.navCtrl.remove(index);
    })
  }

  nfcTag(){
    this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
      if (data && data.tag && data.tag.id) {
        let tagId = this.nfc.bytesToHexString(data.tag.id);
        if (tagId) {
          this.showTag(tagId);
        }
      }
    });


  }

  showTag(tagId){
    this.tagId= tagId


    var body = {
      cardRealId: this.tagId
    }
    this.http.post(this.ipLocal+'attachments/info', body,this.options).map(res => res.json()).subscribe(
        data => {
          console.log(data);
          this.showSum =true
          this.textToShow = 'Le solde de cette carte est : '+data.data.attachment.uniqueId
          this.sum = data.data.attachment.totalAmount;
          this.bonusSum = data.data.attachment.balance.bonusAmount;
          this.expired = data.data.attachment.expiresAt;
          this.realId = data.data.attachment.uniqueId;

        },
        error => {
          console.log("ERROR: ", error)
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: error,
            buttons: ['OK']
          });
          alert.present();
        }
    );

    this.nfc.removeTagDiscoveredListener(this.showTag);
  }
  sesReadNFC(data): void {
    // alert('NFC_WORKING')
  }

}
