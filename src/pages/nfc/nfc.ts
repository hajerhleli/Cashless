import { Component } from '@angular/core';

import { NavController ,LoadingController} from 'ionic-angular';
// import {NFC, Ndef} from 'ionic-native';
import {HomePage} from "../home/home";
@Component({
  selector: 'page-nfc',
  templateUrl: 'nfc.html'
})
export class NFCPPage {
  id;
  wlc_txt = '';
  balanceW;
  balanceSh;
  balanceS;
  constructor(    private _nav: NavController,public loadingCtrl: LoadingController) {
    // console.log('add listener');
    // var thisID = this;
    // NFC.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
    //   if (data && data.tag && data.tag.id) {
    //     let tagId = NFC.bytesToHexString(data.tag.id);
    //     if (tagId) {
    //       alert(tagId);
    //       thisID.id = tagId
    //       localStorage.setItem('tagId',tagId)
    //     }
    //     } else {
    //       alert('NFC_NOT_DETECTED')
    //     }
    //
    // });


  }

//   addListenNFC() {
//     console.log('add listener')
//     NFC.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
//     if (data && data.tag && data.tag.id) {
//       let tagId = NFC.bytesToHexString(data.tag.id);
//       if (tagId) {
//         alert(tagId );
//       } else {
//         alert('NFC_NOT_DETECTED')
//       }
//     }
//   });
// }
  ionViewWillEnter(){
    localStorage.setItem('page','nfc');
    this.id = localStorage.getItem('tagId')
    if(this.id == 'cecd2cbc'){
      //alert('Card Detected with id :'+this.id );
      this.wlc_txt = 'welcome Walid';
      if (localStorage.getItem('balancew') == undefined) {
        this.balanceW = 120;
        localStorage.setItem('balancew', '120');
      }
      else{
        this.balanceW = localStorage.getItem('balancew');
      }
    }
    if(this.id == 'dee22bbc'){
     // alert('Card Detected with id :'+this.id );
      this.wlc_txt = 'welcome Sami';
      if (localStorage.getItem('balances') == undefined) {
        this.balanceW = 300 ;
        localStorage.setItem('balances', '300');
      }
      else{
        this.balanceW = localStorage.getItem('balances');
      }

    }
    if(this.id == '6e112cbc'){
      // alert('Card Detected with id :'+this.id );
      this.wlc_txt = 'welcome Shems';
      alert(localStorage.getItem('balancesh'))
      if (localStorage.getItem('balancesh') == undefined) {
        this.balanceW = 20 ;
        localStorage.setItem('balancesh', '20');
      }
      else{
        this.balanceW = localStorage.getItem('balancesh');
      }

    }

  }
  sesReadNFC(data): void {
    // alert('NFC_WORKING')
  }

  failNFC(err) {
    alert('NFC Failed :' + JSON.stringify(err))
  }


  add(){

    localStorage.setItem('operation','add');
    // alert('add')
    this._nav.push(HomePage);
  }
  sub(){
    // if(this.id == 'cecd2cbc'){
    //   alert('Card Detected with id :'+this.id );
    //   this.wlc_txt = 'welcome Walid';
    //   this.balanceW = 120 ;
    //   localStorage.setItem('balancew', '120');
    // }
    // if(this.id == 'dee22bbc'){
    //   alert('Card Detected with id :'+this.id );
    //   this.wlc_txt = 'welcome Sami';
    //   this.balanceW = 300 ;
    //   localStorage.setItem('balances', '300');
    // }
    // if(this.id == '6e112cbc'){
    //   alert('Card Detected with id :'+this.id );
    //   this.wlc_txt = 'welcome Shems';
    //   this.balanceW = 20 ;
    //   localStorage.setItem('balancesh', '20');
    // }
    localStorage.setItem('operation','sub')
    // alert('sub')
    this._nav.push(HomePage);
  }
}
