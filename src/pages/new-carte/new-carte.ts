import {Component, Input, ViewChild, Output, EventEmitter} from '@angular/core';
import {AlertController, NavController, NavParams, ViewController} from 'ionic-angular';

import {NFC} from "@ionic-native/nfc";
import {ChoicePage} from "../choiceCaissier/choice";
import {Http, Headers, RequestOptions} from "@angular/http";
import {IonDigitKeyboardCmp} from "../../components/ion-digit-keyboard/components/ion-digit-keyboard-cmp/ion-digit-keyboard-cmp";
import {IonDigitKeyboardOptions} from "../ion-digit-keyboard/ion-digit-keyboard";

/**
 * Generated class for the NewCartePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-new-carte',
  templateUrl: 'new-carte.html',
  providers : [NFC]
})
export class NewCartePage {
  @Output() nb = new EventEmitter<string>();
  @Input() nbPerso = '';
  minSolde;
  tagId = '';
  sumKey = '';
  minSoldeToPay = 0;
  defineNb = false;
  constructor(private viewCtrl: ViewController,
              public alertCtrl: AlertController,private http: Http,
              public navCtrl: NavController, public navParams: NavParams, public nfc : NFC) {
    this.nfc = nfc;
  }
  sumTotale  = '';
  sum = 0;
  sumToAdd;
  ipLocal = 'http://cashless.socketon.xyz/api/';
  textPay = 'Payez';
  textPayTip = 'Scan carte';
  textPayScreen = this.textPay;
  textTipScreen = this.textPayTip;
  waitingText = 'Encours de traitement ..';
  waitingTextScan = 'En attente d\'une carte'
  clickedPayT = false;
  disabledT =false;
  disabledP = false;

  //header = new Headers({ 'Authorization' : 'Bearer '+localStorage.getItem('token')});
  options = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')})});


  @ViewChild(IonDigitKeyboardCmp) keyboard;

  userId: string = '';
  userPassword: string = '';
  focus: string = '';

  public keyboardSettings: IonDigitKeyboardOptions = {
    align: 'center',
    //width: '85%',
    visible: true,
    leftActionOptions: {
      iconName: 'ios-backspace-outline',
      fontSize: '1.4em'
    },
    rightActionOptions: {
      iconName: 'ios-checkmark-circle-outline',
      // text: '.',
      fontSize: '1.3em'
    },
    roundButtons: false,
    showLetters: true,
    swipeToHide: false,
    // Available themes: IonDigitKeyboard.themes
    theme: 'dark'
  }

  ionViewWillEnter(){
    /**
     * Since we want to prevent native keyboard to show up, we put the disabled
     * attribute on the input, and manage focus programmaticaly.
     */
    this.keyboard.onClick.subscribe((key: any) => {
      console.log(key)
      if (typeof key == 'number') {
        this.sumKey += key;
        this.changeSum();
      } else {
        if (key == 'left') this.sumKey = this.sumKey.substring(0, this.sumKey.length - 1);
        this.changeSum();
        this.nb.emit(this.nbPerso);
      }
    });

    // (BLur) Clear focus field name on keyboard hide
    this.keyboard.onHide.subscribe(() => {
      this.focus = '';
    });
  }

  changeSum() {
    if (!this.defineNb)
      this.nbPerso = this.sumKey;
    else
      this.sumTotale = this.sumKey;
  }

  setFocus(field: string) {
    this.focus = field;
    this.keyboard.show();
  }

  createOrder() {
    // to test just
    // this.defineFacture = true;
    // this.sumKey = '';
    var body = {
      consumers: parseInt(this.nbPerso)
    }
    //console.log(this.options)
    this.http.post(this.ipLocal + 'attachments/requiredAmountCalculator', body, this.options).map(res => res.json()).subscribe(
        data => {
          console.log(data);
          this.defineNb = true;
          this.minSoldeToPay = data.data.requiredAmountFormat
          this.sumKey = '';
        },
        error => {
          let errorData = JSON.parse(error._body);
          let alert = this.alertCtrl.create({
            title: 'ERREUR',
            subTitle:errorData.data.message,
            buttons: ['OK']
          });
          alert.present();

        }
    );
  }

  nfcTag() {
    this.textPayScreen = this.waitingTextScan;
    if (this.sumTotale == '') {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'SVP tapez le montant',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      //
      this.disabledP = true;
      this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
        if (data && data.tag && data.tag.id) {
          let tagId = this.nfc.bytesToHexString(data.tag.id);
          if (tagId) {
            console.log('before pay')
            this.showTag(tagId);
            this.textPayScreen = this.waitingText;
          }
        }
      });
      // this.showTag('dee22bbc');
      // this.disabledP = true;
    }
  }

  showTag(tagId) {

    console.log(this.sumTotale);
    this.tagId = tagId
    var body = {
      amount: parseInt(this.sumTotale) * 1000,
      cardRealId: this.tagId,
      consumers : this.nbPerso
    }

    this.textPayScreen = this.waitingText;
    console.log(this.tagId)
    if (this.tagId != '') {
      this.http.post(this.ipLocal + 'attachments/charge', body, this.options).map(res => res.json()).subscribe(
          data => {
            console.log(data)
            if (data.code == 74) {
              console.log(data);
              let alert = this.alertCtrl.create({
                title: 'Succès',
                subTitle: 'Montant payé',
                buttons: ['OK']
              });
              alert.present();
              this.textPayScreen = this.textPay;
              this.finish();
            }
          },
          error => {
            let errorData = JSON.parse(error._body);
            let alert = this.alertCtrl.create({
              title: 'ERREUR',
              subTitle:errorData.data.message,
              buttons: ['OK']
            });
            alert.present();
            this.disabledP=false;
            this.textPayScreen = this.textPay;
          }
      );
    } else {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'Scanner une carte SVP',
        buttons: ['OK']
      });
      alert.present();
    }
    this.nfc.removeTagDiscoveredListener(this.showTag)

  }


  sesReadNFC(data): void {
    // alert('NFC_WORKING'+data);
  }

  finish() {
    this.navCtrl.push(ChoicePage).then(() => {
      // first we find the index of the current view controller:
      const index = this.viewCtrl.index;
      // then we remove it from the navigation stack
      this.navCtrl.remove(index);
    })
  }
}
