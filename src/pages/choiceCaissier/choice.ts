import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, AlertController, ViewController} from 'ionic-angular';

// import {NFC} from 'ionic-native';
// import { EventListPage } from "../event-list/event-list";
import {Http} from "@angular/http";
import {NFC} from "@ionic-native/nfc";
import { ScanPage} from "../scan/scan";
import {NewCartePage} from "../new-carte/new-carte";
import {CashoutPage} from "../cashout/cashout";
import {CheckPage} from "../check/check";
import {SoldeRechargePage} from "../solde-recharge/solde-recharge";
// import {NFC} from "@ionic-native/nfc";

@Component({
  selector: 'page-choice',
  templateUrl: 'choice.html',
  providers:[NFC]
})
export class ChoicePage {
  id
  nfc;

  constructor(
    private _nav: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private http:Http,
    private _loadingController: LoadingController,
    public alertCtrl: AlertController, nfc : NFC) {
      // Create FormControl to validate fields
      this.nfc = nfc

  }
  redirectRecharge(){
    this._nav.push(SoldeRechargePage);
  }
  redirectAdd(){
    this._nav.push(NewCartePage);
  }
  redirectCashout(){
    this._nav.push(CashoutPage);
  }
  redirectCheck(){
    this._nav.push(CheckPage);
  }

}
