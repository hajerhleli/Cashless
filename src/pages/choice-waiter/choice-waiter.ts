import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {CheckPage} from "../check/check";
import {FacturePage} from "../facture/facture";
/**
 * Generated class for the ChoiceWaiterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-choice-waiter',
  templateUrl: 'choice-waiter.html',
})
export class ChoiceWaiterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoiceWaiterPage');
  }
  redirectCheck(){
    this.navCtrl.push(CheckPage);
  }
  redirectFacture(){
    this.navCtrl.push(FacturePage);
  }

}
