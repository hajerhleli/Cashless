import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, AlertController, ViewController} from 'ionic-angular';

// import {NFC} from 'ionic-native';
// import { EventListPage } from "../event-list/event-list";
import {Http} from "@angular/http";
import {NFC} from "@ionic-native/nfc";
import {SoldeRechargePage} from "../solde-recharge/solde-recharge";
// import {NFC} from "@ionic-native/nfc";

@Component({
  selector: 'page-recharge',
  templateUrl: 'scan.html',
  providers:[NFC]
})
export class ScanPage {
  id
  nfc;

  constructor(
      private _nav: NavController,
      public navParams: NavParams,
      private viewCtrl: ViewController,
      private http:Http,
      private _loadingController: LoadingController,
      public alertCtrl: AlertController, nfc : NFC) {
    // Create FormControl to validate fields
    this.nfc = nfc

  }
  nfcTag(){

    localStorage.setItem('page','choice');
    console.log('add listener');
    // var thisID = this;
    this._nav.push(SoldeRechargePage);
    // this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
    //   if (data && data.tag && data.tag.id) {
    //     let tagId = this.nfc.bytesToHexString(data.tag.id);
    //     if (tagId) {
    //       //alert('Card Detected with id :'+tagId );
    //       thisID.id = tagId
    //       localStorage.setItem('tagId',tagId)
    //
    //       if (localStorage.getItem('page') == 'choice') {
    //         localStorage.setItem('page','nfc');
    //         this._nav.push(SoldeRechargePage);
    //       }
    //     }
    //   }
    //
    // });
  }
  scan(){
    // this._nav.push(EventListPage)
  }
  sesReadNFC(data): void {
    // alert('NFC_WORKING')
  }

}
