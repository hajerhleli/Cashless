import {Component} from '@angular/core';
import {NavController, NavParams, AlertController, ViewController} from 'ionic-angular';

import {Headers, Http, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {ChoicePage} from "../choiceCaissier/choice";
import {ChoiceWaiterPage} from "../choice-waiter/choice-waiter";
import {ChoiceManagerPage} from "../choice-manager/choice-manager";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    public newUser = {
        username: 'cashier',
        password: 'benarous'
    };
    headers = new Headers({'Content-Type': 'application/json'});
    ipLocal = 'http://cashless.socketon.xyz/api/';
    disable = false;
    constructor(private viewCtrl: ViewController,
                private _nav: NavController,
                public navParams: NavParams,
                private http: Http,
                public alertCtrl: AlertController) {
        // Create FormControl to validate fields

        this.http = http;
    }

    public login() {
        this.disable = true ;
        return new Promise((resolve, reject) => {

            let formData: any = new FormData();
            let xhr = new XMLHttpRequest()
            formData.append("_username", this.newUser.username);
            formData.append("_password", this.newUser.password);
            let th = this;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        console.log(JSON.parse(xhr.response));
                        localStorage.setItem('token', JSON.parse(xhr.response).token);
                        var options = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')})});

                        th.http.get(th.ipLocal+'getUser', options).map(res => res.json()).subscribe(
                            data => {
                                console.log(data.data.user.role);
                                if (data.data.user.role === 'ROLE_WAITER'){
                                    // this._nav.push(ChoicePage).then(() => {
                                    th._nav.push(ChoiceWaiterPage).then(() => {
                                        // this._nav.push(ChoiceManagerPage).then(() => {
                                        // first we find the index of the current view controller:
                                        const index = th.viewCtrl.index;
                                        // then we remove it from the navigation stack
                                        th._nav.remove(index);
                                    })
                                }
                                if (data.data.user.role === 'ROLE_CASHIER'){
                                    th._nav.push(ChoicePage).then(() => {
                                        // this._nav.push(ChoiceManagerPage).then(() => {
                                        // first we find the index of the current view controller:
                                        const index = th.viewCtrl.index;
                                        // then we remove it from the navigation stack
                                        th._nav.remove(index);
                                    })
                                }
                                if (data.data.user.role === 'ROLE_MANAGER'){
                                        th._nav.push(ChoiceManagerPage).then(() => {
                                        // first we find the index of the current view controller:
                                        const index = th.viewCtrl.index;
                                        // then we remove it from the navigation stack
                                        th._nav.remove(index);
                                    })
                                }

                            },
                            error => {
                                console.log("ERROR: ", error)
                                let alert = th.alertCtrl.create({
                                    title: 'Alert',
                                    subTitle: error,
                                    buttons: ['OK']
                                });
                                alert.present();
                            }
                        );





                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", this.ipLocal+'login_check', true);
            console.log(formData)
            xhr.send(formData);
        });
        //
        //
        //         this._nav.push(ChoiceWaiterPage).then(() => {
        //           // this._nav.push(ChoiceManagerPage).then(() => {
        //           //first we find the index of the current view controller:
        //           const index = this.viewCtrl.index;
        //           // then we remove it from the navigation stack
        //           this._nav.remove(index);
        //
        //
        // })

    }
}
