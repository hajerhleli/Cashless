import {Component, ViewChild} from '@angular/core';
import {AlertController, NavController, NavParams, ViewController} from 'ionic-angular';
import {NFC} from "@ionic-native/nfc";
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {IonDigitKeyboardCmp, IonDigitKeyboardOptions} from '../../components/ion-digit-keyboard';
import {ChoicePage} from "../choiceCaissier/choice";

/**
 * Generated class for the SoldeRechargePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-solde-recharge',
  templateUrl: 'solde-recharge.html',
  providers:[NFC]
})
export class SoldeRechargePage {
  sumToPay: string = '';
  sumKey : string = '';
  tagId = '';
  ipLocal = 'http://cashless.socketon.xyz/api/';
  textPay = 'Payez';
  textPayTip = 'Scan carte';
  textPayScreen = this.textPay;
  textTipScreen = this.textPayTip;
  waitingText = 'Encours de traitement ..';
  waitingTextScan = 'En attente d\'une carte'
  clickedPayT = false;
  disabledT =false;
  disabledP = false;
  options = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('token')})});

  constructor(private http: Http, public navCtrl: NavController, public navParams: NavParams,
              public nfc: NFC, private viewCtrl: ViewController, public alertCtrl: AlertController) {
    this.nfc = nfc;
    this.http = http;
  }

  @ViewChild(IonDigitKeyboardCmp) keyboard;

  userId: string = '';
  userPassword: string = '';
  focus: string = '';

  public keyboardSettings: IonDigitKeyboardOptions = {
    align: 'center',
    //width: '85%',
    visible: true,
    leftActionOptions: {
      iconName: 'ios-backspace-outline',
      fontSize: '1.4em'
    },
    rightActionOptions: {
      iconName: 'ios-checkmark-circle-outline',
      // text: '.',
      fontSize: '1.3em'
    },
    roundButtons: false,
    showLetters: true,
    swipeToHide: false,
    // Available themes: IonDigitKeyboard.themes
    theme: 'dark'
  }

  ionViewWillEnter() {
    /**
     * Since we want to prevent native keyboard to show up, we put the disabled
     * attribute on the input, and manage focus programmaticaly.
     */
    this.keyboard.onClick.subscribe((key: any) => {
      console.log(key)
      if (typeof key == 'number') {
        this.sumKey += key;
        this.changeSum();
      } else {
        if (key == 'left') this.sumKey = this.sumKey.substring(0, this.sumKey.length - 1);
        this.changeSum();
      }
      console.log(this.sumToPay)
    });

    // (BLur) Clear focus field name on keyboard hide
    this.keyboard.onHide.subscribe(() => {
      this.focus = '';
    });
  }

  changeSum() {
      this.sumToPay = this.sumKey;
  }

  setFocus(field: string) {
    this.focus = field;
    this.keyboard.show();
  }

  nfcTag() {
    this.textPayScreen = this.waitingTextScan;
    if (this.sumToPay == '') {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'SVP tapez le montant',
        buttons: ['OK']
      });
      alert.present();
    }
    else {

      this.nfc.addTagDiscoveredListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
        if (data && data.tag && data.tag.id) {
          let tagId = this.nfc.bytesToHexString(data.tag.id);
          if (tagId) {
            console.log('before pay')
            this.showTag(tagId);
            this.textPayScreen = this.waitingText;
            this.disabledP = true;
          }
        }
      });
      // this.showTag('dee22bbc');
      this.disabledP = true;
    }
  }

  showTag(tagId) {

    console.log(this.sumToPay);
    this.tagId = tagId
    var body = {
      amount: parseInt(this.sumToPay) * 1000,
      cardRealId: this.tagId
    }
    console.log(this.tagId)
    if (this.tagId != '') {
      this.http.post(this.ipLocal + 'attachments/chargeAdditional', body, this.options).map(res => res.json()).subscribe(
          data => {
            console.log(data)
            if (data.code == 74) {
              console.log(data);
              this.sumKey = '';
              this.tagId = '';
              this.disabledP=false;
              this.textPayScreen = this.textPay;
              let alert = this.alertCtrl.create({
                title: 'Succès',
                subTitle: 'Montant Versé dans la carte',
                buttons: ['OK']
              });
              alert.present();
              this.navCtrl.push(ChoicePage).then(() => {
                // first we find the index of the current view controller:
                const index = this.viewCtrl.index;
                // then we remove it from the navigation stack
                this.navCtrl.remove(index);
              });
            }
          },
          error => {
            let errorData = JSON.parse(error._body);
            let alert = this.alertCtrl.create({
              title: 'ERREUR',
              subTitle:errorData.data.message,
              buttons: ['OK']
            });
            alert.present();
            this.disabledP=false;
            this.textPayScreen = this.textPay;
          }
      );
    } else {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'Scanner une carte SVP',
        buttons: ['OK']
      });
      alert.present();
    }
    this.nfc.removeTagDiscoveredListener(this.showTag)
  //
  }
  sesReadNFC(data): void {
    // alert('NFC_WORKING'+data);
  }




}
